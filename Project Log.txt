Job Manager - AD500 Capstone class

-Week 1:

	1.1: Introduction, reviewed requirements. began drafting documents for S.O.W. Initialize Android Studio project. Setup for VCS, Bitbucket

	1.2: Finish S.O.W. documentation. Commit and push initial code to Bitbucket

	1.3: (Half day) Define project structure, add some layouts for conceptual screens. Research implementation of tab navigation, implemented in MainActivity

-Week 2:

	2.1: 2 minute status update. Implement fragments for tabs layout (Working in current build). Research floating action button menu, would like to implement. Designed Listener interface for each model. Refactored for "Logs" tab. Implementation for ExpandableListView's.

	2.2: Created Entity Relationship Diagram, and generated MySQL statements, to be converted. A bit of Refactoring in project structure. Built on Listener interfaces.

	2.3: Refactored Listener interface to an abstract class with static methods. Implemented menus into application to aide in UI. Designed use case diagrams for overall UI, adding NewLog Activity to allow for input of text to Log. Extensive UI design and theming research...

-Week 3:

	3.1: Added FAB button, and action icons for 'new' and 'share'. Class diagram and navigation sequence diagram. Need more use case sequence diagrams. Current build working with TabAdapter and PreferenceFragment

	3.2: Begin building ExpandableListView and Adapters. Customize UI with Style Generator. FAB changes color onClick to track clock in/out. Added SharedPreferences

	3.3: Implemented Adapters, Listeners, and Fragments. Testing with a dummy list of jobNames. UI is working as expected so far. Setup statements for DBHelper class, test run.

-Week 4:

	4.1: Database work

	4.2: (Work from home) Shared Preferences and database work

	4.3: Added preferences to keep last job and day worked, implemented checking to allow for clocking in. App will now allow for clocking into a job, however, need to implement persistence.

-Week 5:

	5.1: Crud and persistence methods

	5.2: Clock in and out functions

	5.3: Bug fixing

-Week 6:

	6.1: Bug fixing and refinement, time calculation methods

	6.2: Display data to UI elements, bug fixing, refinement. Handle list clicks

	6.3: Revert to API 19 for styles. A little polish. Presentations