package com.imherolddev.jobmanagerpro;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.imherolddev.jobmanagerpro.fragments.TabsPagerAdapter;
import com.imherolddev.jobmanagerpro.model.Day;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.ListenerUtility;
import com.imherolddev.jobmanagerpro.model.Log;
import com.imherolddev.jobmanagerpro.model.persistence.JMCrudServiceImpl;
import com.imherolddev.jobmanagerpro.model.persistence.JMDBHelper;
import com.imherolddev.jobmanagerpro.model.persistence.PreferenceHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by imherolddev on 9/20/2014.
 */
public class MainActivity extends ActionBarActivity implements ActionBar.TabListener, PopupMenu.OnMenuItemClickListener, ListenerUtility.JobListListener, ListenerUtility.DayListListener, ListenerUtility.LogListListener {

    ////MainActivity variables
    private ActionBar actionBar;
    //private Toolbar toolbar;
    private ViewPager viewPager;
    private TabsPagerAdapter tabsPagerAdapter;

    private ArrayList<Day> dayArrayList = new ArrayList<>();
    private ArrayList<Job> jobArrayList = new ArrayList<>();
    private ArrayList<Log> logArrayList = new ArrayList<>();

    private JMCrudServiceImpl jmCrudService;

    private PreferenceHelper preferenceHelper;

    private Button fab;

    private boolean isClocked = false;
    private boolean clockNewJob = false;

    private Job lastJob;
    private Calendar lastCalendar = new GregorianCalendar();
    private Day lastDay;

    ////Keys for initializing Lists
    private final int JOB_LIST = 0;
    private final int DAY_LIST = 1;
    private final int LOG_LIST = 2;
////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instantiate preferences and database
        preferenceHelper = new PreferenceHelper(this);
        initDB();

        //set-up actionbar and tabs pager
        //TODO - update to non-deprecated tabs methods for support v21
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }*/
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabsPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int i, float v, int i2) {
                //empty
            }

            @Override
            public void onPageSelected(int i) {
                actionBar.setSelectedNavigationItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                //empty
            }

        });

        //Name tabs
        //TODO - update to non-deprecated tabs methods for support v21
        String[] tabNames = getResources().getStringArray(R.array.tab_name_array);
        for (String tab_name : tabNames) {
            actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
        }

        //Initialize Floating Action Button (FAB)
        //Set to current clock state
        fab = (Button) findViewById(R.id.fab);
        changeClockState();

    }

    @Override
    public void onResume() {

        super.onResume();

        //Initialize data sets
        //notify tabs adapter
        initArrayLists();
        initLastItems();
        changeClockState();
        tabsPagerAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //Handle actionbar item clicks
        switch (item.getItemId()) {

            case R.id.action_settings:
                startActivity(new Intent(this, MainSettings.class));
                return true;

            case R.id.action_add:
                PopupMenu popupMenu = new PopupMenu(this, findViewById(R.id.action_add));
                popupMenu.inflate(R.menu.main_add);
                popupMenu.setOnMenuItemClickListener(this);
                popupMenu.show();

                return true;

            case R.id.action_send:
                // TODO - add send menu and associated actions
                //Dialog: What to send?
                //Gather data
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        //Handle popup menu clicks
        switch (item.getItemId()) {

            case R.id.action_add_job:

                getNewJobDialog().show();

                return true;

            case R.id.action_add_log:

                //Start new Log activity for result: new Log
                Intent intent = new Intent(this, NewLog.class);
                Bundle extras = new Bundle();
                String[] jobNameArray = bundleJobNames();
                extras.putStringArray(NewLog.JOB_BUNDLE, jobNameArray);
                intent.putExtras(extras);
                startActivityForResult(intent, NewLog.ADD_LOG_REQUEST);

                return true;

            case R.id.action_add_photo:
                //TODO - add photo capability to Logs
                //call camera, take photo, attach to new Log
                return true;

            default:
                return false;

        }

    }

    //Floating Action Button clicked
    public void fabClick(View v) {

        //Test clock state
        if (!isClocked) {

            //Test job list size
            if (jobArrayList.size() > 0) {

                //AlertDialog to display selection of existing Jobs
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.select_job_title);

                builder.setItems(bundleJobNames(), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Get job at 'which' to clock new time
                        Calendar calendar = new GregorianCalendar();
                        clockTime(jobArrayList.get(which), calendar);

                    }

                });

                //New Job button - clock new Job
                builder.setPositiveButton(R.string.new_job, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Send clockNewJob to NewJobDialog
                        clockNewJob = true;
                        getNewJobDialog().show();

                    }
                });

                builder.show();

            } else {

                //No Jobs created
                //Send clockNewJob to NewJobDialog
                clockNewJob = true;
                getNewJobDialog().show();

            }

        } else {
            //Already clocked to lastJob, add clock out
            clockTime(lastJob, new GregorianCalendar());
        }

    }

    ////Override TabListener
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        viewPager.setCurrentItem(tab.getPosition(), true);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        //empty
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        //empty
    }
////////

    ////Override listListeners
    @Override
    public ArrayList<Job> getJobList() {
        return jobArrayList;
    }

    @Override
    public ArrayList<Log> getLogList() {
        return logArrayList;
    }

    @Override
    public ArrayList<Day> getDayList() {
        return dayArrayList;
    }
////////

    //Change FAB appearance by clock state
    private void changeClockState() {

        if (isClocked) {
            fab.setBackgroundResource(R.drawable.fab_out);
        } else {
            fab.setBackgroundResource(R.drawable.fab_in);
        }

    }

    //Bundle Job names to AutoCompleTextView in NewLog
    private String[] bundleJobNames() {

        String[] jobNamesArray = new String[jobArrayList.size()];

        for (Job job : jobArrayList) {

            jobNamesArray[jobArrayList.indexOf(job)] = job.getJobName();

        }

        return jobNamesArray;

    }

    //Toasty!!!
    private void toast(int id) {

        Toast.makeText(this, getString(id), Toast.LENGTH_LONG).show();

    }

    //Setup of database and CRUD service
    private void initDB() {

        JMDBHelper jmdbHelper = new JMDBHelper(this, JMDBHelper.DB_NAME, null, 1);
        SQLiteDatabase db = jmdbHelper.getWritableDatabase();

        jmCrudService = new JMCrudServiceImpl(db);

    }

    //Initialize ArrayLists to serve data to tab Fragments
    private void initArrayLists() {

        jobArrayList = jmCrudService.readJobList();
        dayArrayList = jmCrudService.readDayList();
        sortDaysToJobs();
        logArrayList = jmCrudService.readLogList();
        sortLogsToJobs();

        tabsPagerAdapter.notifyDataSetChanged();

    }

    //Initialize only the selected lists
    private void initArrayLists(Integer... ints) {

        for (Integer choice : Arrays.asList(ints)) {

            switch (choice) {

                case JOB_LIST:
                    jobArrayList = jmCrudService.readJobList();
                    break;

                case DAY_LIST:
                    dayArrayList = jmCrudService.readDayList();
                    sortDaysToJobs();
                    break;

                case LOG_LIST:
                    logArrayList = jmCrudService.readLogList();
                    sortLogsToJobs();
                    break;

            }

            tabsPagerAdapter.notifyDataSetChanged();

        }

    }

    //Retrieve last item: clock state, last Job, last Day, last Calendar
    private void initLastItems() {

        isClocked = preferenceHelper.retrieveFlag(PreferenceHelper.CLOCKED_KEY);

        if (jobArrayList.size() > 0) {

            for (Job job : jobArrayList) {
                if (job.getJobId() == preferenceHelper.retrieve(PreferenceHelper.LAST_JOB_KEY)) {
                    lastJob = job;
                    break;
                }
            }

        }

        Calendar calendar = new GregorianCalendar();

        if (dayArrayList.size() > 0) {

            calendar.setTimeInMillis(preferenceHelper.retrieveDate(PreferenceHelper.LAST_DAY_KEY));

            for (Day day : dayArrayList) {

                if (day.getDate().equals(calendar)) {
                    lastDay = day;
                    break;
                }

            }

        }

        lastCalendar.setTimeInMillis(calendar.getTimeInMillis());

    }

    //Dialog for creating a new Job
    private AlertDialog.Builder getNewJobDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.tv_job);

        final EditText et = new EditText(this);
        builder.setView(et);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                boolean unique = true;
                String jobName = et.getText().toString();

                if (!jobName.equals("")) {

                    for (Job j : jobArrayList) {

                        if (jobName.equals(j.getJobName())) {
                            unique = false;
                            break;
                        }

                    }

                    if (unique) {

                        Job job = new Job(et.getText().toString());
                        if (jmCrudService.createJob(job)) {
                            job = jmCrudService.readJob(jobName);
                        }
                        initArrayLists(JOB_LIST);
                        lastJob = job;
                        Calendar calendar = new GregorianCalendar();

                        if (clockNewJob) {
                            clockTime(job, calendar);
                        }

                    } else {
                        toast(R.string.unique_job_name);
                    }

                } else {
                    toast(R.string.no_text);
                }

            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //empty, to dismiss
            }
        });

        return builder;

    }

    //On return from Activity for result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            //New Log
            case NewLog.ADD_LOG_REQUEST:

                if (resultCode == RESULT_OK) {

                    Log log = (Log) data.getSerializableExtra(NewLog.LOG_EXTRA);
                    log.setjobId(jobArrayList.get(data.getIntExtra(NewLog.JOB_POS_EXTRA, 0)).getJobId());

                    for (Job job : jobArrayList) {
                        if (log.getjobId() == job.getJobId()) {
                            job.addLogToList(log);
                            break;
                        }
                    }

                    jmCrudService.createLog(log);
                    initArrayLists(JOB_LIST, LOG_LIST);

                }

        }

    }

    //Clock time to Job at Calendar
    private void clockTime(Job job, Calendar calendar) {

        if (job.getJobId() == lastJob.getJobId()) {

            if (lastCalendar.equals(calendar) && !clockNewJob) {

                lastDay.addClockTime(lastJob.getJobId(),
                        calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
                jmCrudService.createClockTime(lastJob.getJobId(), calendar);

            } else {

                lastDay = newDay(calendar, lastJob);
            }

        } else if (lastCalendar.equals(calendar)) {

            lastJob = job;
            lastDay.addClockTime(lastJob.getJobId(),
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            jmCrudService.createClockTime(lastJob.getJobId(), calendar);

        } else {

            lastJob = job;
            lastDay = newDay(calendar, lastJob);

        }

        isClocked = !isClocked;
        preferenceHelper.save(PreferenceHelper.CLOCKED_KEY, isClocked);
        lastJob = job;
        preferenceHelper.save(PreferenceHelper.LAST_JOB_KEY, lastJob.getJobId());
        lastCalendar.setTimeInMillis(calendar.getTimeInMillis());
        preferenceHelper.save(PreferenceHelper.LAST_DAY_KEY, lastCalendar);
        changeClockState();
        initArrayLists();

    }

    //New Day at Calendar to Job
    private Day newDay(Calendar calendar, Job job) {

        Day day = new Day(job, calendar);
        jmCrudService.createClockTime(job.getJobId(), calendar);
        lastDay = day;
        initArrayLists();
        return day;

    }

    //Sort Logs into Jobs
    private void sortLogsToJobs() {

        for (Log log : logArrayList) {

            for (Job job : jobArrayList) {

                if (log.getjobId() == job.getJobId()) {
                    job.addLogToList(log);
                }

            }

        }

    }

    //Sort Days to Jobs
    private void sortDaysToJobs() {

        for (Job job : jobArrayList) {

            for (Day day : dayArrayList) {

                for (Integer id : day.getJobIdList()) {

                    if (id == job.getJobId()) {
                        job.addDayToList(day);
                        break;
                    }

                }

            }

        }

    }

}