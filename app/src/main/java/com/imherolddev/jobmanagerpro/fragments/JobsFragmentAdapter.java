package com.imherolddev.jobmanagerpro.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.imherolddev.jobmanagerpro.R;
import com.imherolddev.jobmanagerpro.model.Day;
import com.imherolddev.jobmanagerpro.model.Job;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by imherolddev on 9/22/2014.
 */
public class JobsFragmentAdapter extends BaseExpandableListAdapter {

    private ArrayList<Job> jobArrayList;
    private ArrayList<Day> dayArrayList;

    private LayoutInflater layoutInflater;

    public JobsFragmentAdapter(Activity activity, ArrayList<Job> jobArrayList, ArrayList<Day> dayArrayList) {

        this.jobArrayList = jobArrayList;
        this.dayArrayList = dayArrayList;

        layoutInflater = activity.getLayoutInflater();

    }

    /**
     * Gets the number of groups.
     *
     * @return the number of groups
     */
    @Override
    public int getGroupCount() {
        return jobArrayList.size();
    }

    /**
     * Gets the number of children in a specified group.
     *
     * @param groupPosition the position of the group for which the children
     *                      count should be returned
     * @return the children count in the specified group
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return jobArrayList.get(groupPosition).getDayArrayList().size();
    }

    /**
     * Gets the data associated with the given group.
     *
     * @param groupPosition the position of the group
     * @return the data child for the specified group
     */
    @Override
    public Object getGroup(int groupPosition) {
        return jobArrayList.get(groupPosition);
    }

    /**
     * Gets the data associated with the given child within the given group.
     *
     * @param groupPosition the position of the group that the child resides in
     * @param childPosition the position of the child with respect to other
     *                      children in the group
     * @return the data of the child
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return jobArrayList.get(groupPosition).getDayArrayList().get(childPosition);
    }

    /**
     * Gets the ID for the group at the given position. This group ID must be
     * unique across groups. The combined ID (see
     * {@link #getCombinedGroupId(long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group for which the ID is wanted
     * @return the ID associated with the group
     */
    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    /**
     * Gets the ID for the given child within the given group. This ID must be
     * unique across all children within the group. The combined ID (see
     * {@link #getCombinedChildId(long, long)}) must be unique across ALL items
     * (groups and all children).
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group for which
     *                      the ID is wanted
     * @return the ID associated with the child
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    /**
     * Indicates whether the child and group IDs are stable across changes to the
     * underlying data.
     *
     * @return whether or not the same ID always refers to the same object
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Gets a View that displays the given group. This View is only for the
     * group--the Views for the group's children will be fetched using
     * {@link #getChildView(int, int, boolean, android.view.View, android.view.ViewGroup)}.
     *
     * @param groupPosition the position of the group for which the View is
     *                      returned
     * @param isExpanded    whether the group is expanded or collapsed
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     *                      {@link #getGroupView(int, boolean, android.view.View, android.view.ViewGroup)}.
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the group at the specified position
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_group, parent, false);
        }

        ((CheckedTextView) convertView).setText(((Job) getGroup(groupPosition)).getJobName());
        ((CheckedTextView) convertView).setChecked(isExpanded);

        return convertView;

    }

    /**
     * Gets a View that displays the data for the given child within the given
     * group.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child (for which the View is
     *                      returned) within the group
     * @param isLastChild   Whether the child is the last child within the group
     * @param convertView   the old view to reuse, if possible. You should check
     *                      that this view is non-null and of an appropriate type before
     *                      using. If it is not possible to convert this view to display
     *                      the correct data, this method can create a new view. It is not
     *                      guaranteed that the convertView will have been previously
     *                      created by
     *                      {@link #getChildView(int, int, boolean, android.view.View, android.view.ViewGroup)}.
     * @param parent        the parent that this view will eventually be attached to
     * @return the View corresponding to the child at the specified position
     */
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listrow_details_jobs, parent, false);
        }

        Job job = (Job) getGroup(groupPosition);
        Day day = (Day) getChild(groupPosition, childPosition);

        TextView date = (TextView) convertView.findViewById(R.id.tv_date);
        TextView hours = (TextView) convertView.findViewById(R.id.tv_hours);

        if (day == null) {

            convertView = layoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);

            TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
            textView.setText(R.string.tv_no_days);

        } else {

            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");
            date.setText(dateFormat.format(day.getDate().getTimeInMillis()));

            hours.setText(getHours(job.getClockTimes(day.getDate())));

        }

        return convertView;

    }

    /**
     * Whether the child at the specified position is selectable.
     *
     * @param groupPosition the position of the group that contains the child
     * @param childPosition the position of the child within the group
     * @return whether the child is selectable.
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        //true for editing?
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    private String getHours(ArrayList<Day.ClockTime> clockTimes) {

        String returnString;
        int count = clockTimes.size();
        int hours = 0;
        int minutes = 0;
        Day.ClockTime lastClock = null;
        Day.ClockTime clock;
        int[] hoursMinutes;

        if (count >= 2) {

            for (Day.ClockTime clockTime : clockTimes) {

                clock = clockTime;

                if ((clockTimes.indexOf(clockTime) + 1) % 2 == 0) {

                    hoursMinutes = incrementTime(hours, minutes,
                            clock.getHour() - lastClock.getHour(), clock.getMinute() - lastClock.getMinute());
                    hours = hoursMinutes[0];
                    minutes = hoursMinutes[1];

                }

                lastClock = clockTime;

            }

        }

        if (count % 2 == 1) {

            Calendar calendar = new GregorianCalendar();
            Day.ClockTime clockTime = clockTimes.get(count -1);

            hoursMinutes = incrementTime(hours, minutes,
                    calendar.get(Calendar.HOUR_OF_DAY) - clockTime.getHour(), calendar.get(Calendar.MINUTE) - clockTime.getMinute());
            hours = hoursMinutes[0];
            minutes = hoursMinutes[1];

        }

        returnString = hours + " hours, " + minutes + " minutes";
        return returnString;

    }

    private int[] incrementTime(int startHours, int startMinutes, int hours, int minutes) {

        int[] hoursMinutes = new int[2];

        startHours += hours;
        startMinutes += ((hours * 60) + minutes) % 60;

        hoursMinutes[0] = startHours;
        hoursMinutes[1] = startMinutes;

        return hoursMinutes;

    }

}
