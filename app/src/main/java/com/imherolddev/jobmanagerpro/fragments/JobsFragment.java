package com.imherolddev.jobmanagerpro.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.imherolddev.jobmanagerpro.R;
import com.imherolddev.jobmanagerpro.model.Day;
import com.imherolddev.jobmanagerpro.model.Job;
import com.imherolddev.jobmanagerpro.model.ListenerUtility;

import java.util.ArrayList;

/**
 * Created by imherolddev on 9/22/2014.
 */
public class JobsFragment extends Fragment {

    private ArrayList<Job> jobArrayList = new ArrayList<>();
    private ArrayList<Day> dayArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ListenerUtility.JobListListener jobListListener = (ListenerUtility.JobListListener) getActivity();
        jobArrayList = jobListListener.getJobList();
        ListenerUtility.DayListListener dayListListener = (ListenerUtility.DayListListener) getActivity();
        dayArrayList = dayListListener.getDayList();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.fragment_jobs, container, false);

        ExpandableListView listView_job = (ExpandableListView) convertView.findViewById(R.id.jobsList);
        JobsFragmentAdapter jobsFragmentAdapter = new JobsFragmentAdapter(getActivity(), jobArrayList, dayArrayList);
        listView_job.setAdapter(jobsFragmentAdapter);

        return convertView;

    }

}
