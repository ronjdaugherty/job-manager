package com.imherolddev.jobmanagerpro.model;

import java.util.ArrayList;

/**
 * Created by imherolddev on 9/26/2014.
 */
public abstract class ListenerUtility {

    public static interface JobListListener {

        public ArrayList<Job> getJobList();

    }

    public static interface DayListListener {

        public ArrayList<Day> getDayList();

    }

    public static interface LogListListener {

        public ArrayList<Log> getLogList();

    }

}
