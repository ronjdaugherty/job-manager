package com.imherolddev.jobmanagerpro.model;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by imherolddev on 8/14/2014.
 */
public class Job {

    /**
     * The job id
     */
    private int jobId;
    /**
     * The job name
     */
    private String jobName;
    /**
     * The list of days for the job
     */
    private ArrayList<Day> dayArrayList;
    /**
     * The list of logs for the job
     */
    private ArrayList<Log> logArrayList;

    /**
     * Must be instantiated with
     * @param jobName the job name to set
     */
    public Job(String jobName) {

        this.jobName = jobName;
        this.dayArrayList = new ArrayList<>();
        this.logArrayList = new ArrayList<>();

    }

    /**
     * Get the job id
     * @return jobId
     */
    public int getJobId() {
        return jobId;
    }

    /**
     * The id to set
     */
    public void setJobId(int id) {
        this.jobId = id;
    }

    /**
     * The job name can be changed
     * @param jobName the job name to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * Get the job name
     * @return jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * Get the Day List
     */
    public ArrayList<Day> getDayArrayList() {
        return dayArrayList;
    }

    /**
     * Add Day to List
     */
    public void addDayToList(Day day) {
        dayArrayList.add(day);
    }

    /**
     * Get the log list
     * @return logArrayList
     */
    public ArrayList<Log> getLogArrayList() {
        return logArrayList;
    }

    /**
     * Add Log to the list
     * @param log the Log to set
     */
    public void addLogToList(Log log) {
        logArrayList.add(0, log);
    }

    /**
     * Get ClockTimes associated with Job
     * @return clockTimeList
     */
    public ArrayList<Day.ClockTime> getClockTimes(Calendar date) {

        ArrayList<Day.ClockTime> returnList = new ArrayList<>();
        Day day = new Day(date);

        for (Day d : dayArrayList) {

            if (d.getDate().equals(date)) {
                day = d;
                break;
            }

        }

        for (Day.ClockTime clockTime : day.getClockTimeList()) {

            if (clockTime.getJobId() == this.getJobId()) {
                returnList.add(clockTime);
            }

        }

        return returnList;

    }

}
