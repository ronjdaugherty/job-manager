package com.imherolddev.jobmanagerpro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.imherolddev.jobmanagerpro.model.Log;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class NewLog extends ActionBarActivity {

    public static final int ADD_LOG_REQUEST = 1;
    public static final int ADD_PHOTO_REQUEST = 2;
    public static final int EDIT_LOG_REQUEST = 3;
    public static final String LOG_EXTRA = "log";
    public static final String EDIT_LOG_KEY = "editLog";
    public static final String EDIT_LOG_NAME = "logName";
    public static final String JOB_POS_EXTRA = "jobPos";
    public static final String LOG_BACK_PRESSED = "logBackPressed";


    private Intent returnIntent;
    private Bundle extras;
    private Log edit_log;

    public static final String JOB_BUNDLE = "jobNames";
    private String[] jobNameArray;

    private AutoCompleteTextView et_job_name;
    private EditText et_title;
    private EditText et_entry;

    private static final String SAVE_LOG_NAME = "name";
    private static final String SAVE_LOG_TITLE = "title";
    private static final String SAVE_LOG_ENTRY = "entry";

    private boolean isEdited = false;
    private Calendar preEditDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_log);

        String jobName = "";
        et_job_name = (AutoCompleteTextView) findViewById(R.id.et_job_name);
        et_title = (EditText) findViewById(R.id.et_title);
        et_entry = (EditText) findViewById(R.id.et_entry);

        if (savedInstanceState != null) {

            et_job_name.setText(savedInstanceState.getString(SAVE_LOG_NAME));
            et_title.setText(savedInstanceState.getString(SAVE_LOG_TITLE));
            et_entry.setText(savedInstanceState.getString(SAVE_LOG_ENTRY));

        }

        extras = getIntent().getExtras();

        if (extras != null) {

            if (extras.containsKey(JOB_BUNDLE)) {
                jobNameArray = extras.getStringArray(JOB_BUNDLE);
            }

            if (extras.containsKey(EDIT_LOG_KEY)) {

                isEdited = true;
                edit_log = (Log) extras.getSerializable(EDIT_LOG_KEY);
                jobName = extras.getString(EDIT_LOG_NAME);
                preEditDate = edit_log.getLogDate();

            }

        }

        if (jobNameArray != null) {

            ArrayAdapter<String> arrayAdapter =
                    new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, jobNameArray);
            et_job_name.setAdapter(arrayAdapter);

        }

        if (edit_log != null) {

            et_job_name.setText(jobName);
            et_title.setText(edit_log.getLogTitle());
            et_entry.setText(edit_log.getLogText());

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here.
        switch (item.getItemId()) {

            case R.id.action_save_log:

                Log log = new Log(new GregorianCalendar(),
                        getText(et_title), getText(et_entry));

                if (isEdited) {
                    log.setLogDate(preEditDate);
                }

                returnIntent = new Intent();
                returnIntent.putExtra(LOG_EXTRA, log);
                int selectedId = Arrays.asList(jobNameArray).indexOf(getText(et_job_name));
                returnIntent.putExtra(JOB_POS_EXTRA, selectedId);
                setResult(RESULT_OK, returnIntent);
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString(SAVE_LOG_NAME, getText(et_job_name));
        savedInstanceState.putString(SAVE_LOG_TITLE, getText(et_title));
        savedInstanceState.putString(SAVE_LOG_ENTRY, getText(et_entry));


    }

    @Override
    public void onBackPressed() {

        returnIntent = new Intent().putExtra(LOG_BACK_PRESSED, true);
        setResult(RESULT_CANCELED, returnIntent);

        super.onBackPressed();

    }

    private String getText(EditText et) {

        return et.getText().toString();

    }

}
